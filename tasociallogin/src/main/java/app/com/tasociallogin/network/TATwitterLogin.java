package app.com.tasociallogin.network;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.TwitterAuthProvider;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import app.com.tasociallogin.LoginTypeEnum.TALoginType;
import app.com.tasociallogin.R;
import app.com.tasociallogin.listener.OnLoginCompleteListener;


public class TATwitterLogin {


    private static TwitterAuthClient client;

    private static OnLoginCompleteListener listener;

    private static FirebaseAuth mAuth = TASocialLogin.getAuth();

    public static void requestLogin(final Activity activity, String consumerKey,
                                    String consumerSecret, OnLoginCompleteListener onLoginCompleteListener) {
        listener = onLoginCompleteListener;
        TASocialLogin.setLoginType(TALoginType.TATWITTER);
        if (mAuth.getCurrentUser() == null) {
            TwitterAuthConfig authConfig = new TwitterAuthConfig(consumerKey, consumerSecret);
            TwitterConfig config = new TwitterConfig.Builder(activity.getApplicationContext()).logger(new DefaultLogger(Log.DEBUG)).twitterAuthConfig(authConfig).build();
            Twitter.initialize(config);
            loginCallback(activity);
        } else {
            FirebaseUser firebaseUser = mAuth.getCurrentUser();
            loggedInUser(firebaseUser);
        }
    }

    private static void loginCallback(final Activity activity) {
        client = new TwitterAuthClient();
        client.authorize(activity, new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterSession session = result.data;
                handleTwitterSession(activity,session);
            }

            @Override
            public void failure(TwitterException e) {
                listener.onLoginError(e.getMessage());
            }
        });
    }

    private static void loggedInUser(FirebaseUser user) {
        AccessToken accessToken = new AccessToken.Builder(user.getProviderId())
                .userName(user.getDisplayName())
                .userId(String.valueOf(user.getUid()))
                .profilePictur(String.valueOf(user.getPhotoUrl()))
                .email(user.getEmail())
                .mobileNo(user.getPhoneNumber())
                .build();
        listener.onLoginSuccess(accessToken);
    }

    public static void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (client != null)
            client.onActivityResult(requestCode, resultCode, data);
    }


    private static void handleTwitterSession(final Activity activity, final TwitterSession session) {
        AuthCredential credential = TwitterAuthProvider.getCredential(session.getAuthToken().token, session.getAuthToken().secret);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            assert user != null;
                            loggedInUser(user);
                        } else {
                            listener.onLoginError(activity.getResources().getString(R.string.auth_failed));
                        }
                    }
                });
    }
}
