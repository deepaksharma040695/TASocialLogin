package app.com.tasociallogin.Permission;

public interface PermissionListener {
    void onPermissionGranted(PermissionListener mPerpermissionListener);
    void onPermissionDenied(PermissionListener mPerpermissionListener);
}
