package app.com.tasociallogin.Permission;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TAPermission {
    private static final int PERMISSION_CODE = 1212;
    private static Activity mActivity;
    private static List<String> mCustomPermission = null;
    private static PermissionListener mPerpermissionListener;
    private static String cancelledAndCanRequest;
    private static String cancelledAndCannotRequest;

    /**
     * Check if version is marshmallow and above.
     * Used in deciding to ask runtime permission
     * @param activity           reference of activity
     * @param permissionListener
     * @param permissions        - bundle of permissions
     */
    public static boolean requestPermission(@NonNull Activity activity, @NonNull PermissionListener permissionListener, @NonNull List<String> permissions) {
        mActivity = activity;
        mPerpermissionListener = permissionListener;
        mCustomPermission = permissions;
        cancelledAndCanRequest = null;
        cancelledAndCannotRequest = null;
        if (Build.VERSION.SDK_INT >= 23) {
            List<String> listPermissionsNeeded = permissions;
            List<String> listPermissionsAssign = new ArrayList<>();
            for (String per : listPermissionsNeeded) {
                if (ContextCompat.checkSelfPermission(activity.getApplicationContext(), per) != PackageManager.PERMISSION_GRANTED) {
                    listPermissionsAssign.add(per);
                }
            }
            if (!listPermissionsAssign.isEmpty()) {
                ActivityCompat.requestPermissions(activity, listPermissionsAssign.toArray(new String[listPermissionsAssign.size()]), PERMISSION_CODE);
                return false;
            }
        }
        return true;
    }

    /**
     * Check if version is marshmallow and above.
     * Used in deciding to ask runtime permission
     * @param activity                     - reference of activity
     * @param permissionListener
     * @param permissions                  - bundle of permissions
     * @param cancelledAndCanRequestMsg    - permission denied message
     * @param cancelledAndCannotRequestMsg - permission no longer visiable message
     * @return
     */
    public static boolean requestPermission(@NonNull Activity activity, @NonNull PermissionListener permissionListener, @NonNull List<String> permissions, String cancelledAndCanRequestMsg, String cancelledAndCannotRequestMsg) {
        mActivity = activity;
        mPerpermissionListener = permissionListener;
        mCustomPermission = permissions;
        cancelledAndCanRequest = cancelledAndCanRequestMsg;
        cancelledAndCannotRequest = cancelledAndCannotRequestMsg;
        if (Build.VERSION.SDK_INT >= 23) {
            List<String> listPermissionsNeeded = permissions;
            List<String> listPermissionsAssign = new ArrayList<>();
            for (String per : listPermissionsNeeded) {
                if (ContextCompat.checkSelfPermission(activity.getApplicationContext(), per) != PackageManager.PERMISSION_GRANTED) {
                    listPermissionsAssign.add(per);
                }
            }
            if (!listPermissionsAssign.isEmpty()) {
                ActivityCompat.requestPermissions(activity, listPermissionsAssign.toArray(new String[listPermissionsAssign.size()]), PERMISSION_CODE);
                return false;
            }
        }
        return true;
    }

    /**
     * Check if version is marshmallow and above.
     * Used in deciding to ask runtime permission
     * @param activity           referance of activity
     * @param permissionListener
     * @param permissions        - permission
     */
    public static boolean requestPermission(@NonNull Activity activity, @NonNull PermissionListener permissionListener, @NonNull String permissions) {
        mActivity = activity;
        mPerpermissionListener = permissionListener;
        cancelledAndCanRequest = null;
        cancelledAndCannotRequest = null;
        mCustomPermission = Arrays.asList(new String[]{permissions});
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(activity, permissions) != PackageManager.PERMISSION_GRANTED) {
//                askRequestPermissions(new String[]{permissions});
                ActivityCompat.requestPermissions(activity, new String[]{permissions}, PERMISSION_CODE);
                return false;
            } else {
                // Permission already granted.
//                mPerpermissionListener.onPermissionGranted();
            }
        }
        return true;
    }

    /**
     * Check if version is marshmallow and above.
     * Used in deciding to ask runtime permission
     * @param activity                     - reference of activity
     * @param permissionListener
     * @param permissions                  - permission
     * @param cancelledAndCanRequestMsg    - permission denied message
     * @param cancelledAndCannotRequestMsg - permission no longer visiable message
     * @return
     */
    public static boolean requestPermission(@NonNull Activity activity, @NonNull PermissionListener permissionListener, @NonNull String permissions, String cancelledAndCanRequestMsg, String cancelledAndCannotRequestMsg) {
        mActivity = activity;
        mPerpermissionListener = permissionListener;
        cancelledAndCanRequest = cancelledAndCanRequestMsg;
        cancelledAndCannotRequest = cancelledAndCannotRequestMsg;
        mCustomPermission = Arrays.asList(new String[]{permissions});
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(activity, permissions) != PackageManager.PERMISSION_GRANTED) {
//                askRequestPermissions(new String[]{permissions});
                ActivityCompat.requestPermissions(activity, new String[]{permissions}, PERMISSION_CODE);
                return false;
            } else {
                // Permission already granted.
//                mPerpermissionListener.onPermissionGranted();
            }
        }
        return true;
    }

    /**
     * Check permission result
     * @param requestCode
     * @param permissions  - all permission
     * @param grantResults
     */
    public static void onRequestPermissionsResult(@NonNull int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CODE: {
                List<String> listPermissionsNeeded = mCustomPermission;
                Map<String, Integer> perms = new HashMap<>();
                for (String permission : listPermissionsNeeded) {
                    perms.put(permission, PackageManager.PERMISSION_GRANTED);
                }
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    boolean isAllGranted = true;
                    for (String permission : listPermissionsNeeded) {
                        if (perms.get(permission) == PackageManager.PERMISSION_DENIED) {
                            isAllGranted = false;
                            break;
                        }
                    }
                    if (isAllGranted) {
                        mPerpermissionListener.onPermissionGranted(mPerpermissionListener);
                    } else {
                        boolean shouldRequest = false;
                        for (String permission : listPermissionsNeeded) {
                            if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permission)) {
                                shouldRequest = true;
                                break;
                            }
                        }
                        if (shouldRequest) {
                            ifCancelledAndCanRequest(mActivity);
                        } else {
                            //permission is denied (and never ask again is  checked)
                            //shouldShowRequestPermissionRationale will return false
                            ifCancelledAndCannotRequest(mActivity);
                        }
                    }
                }
            }
        }
    }

    /**
     * permission cancel dialog
     * @param activity
     */
    private static void ifCancelledAndCanRequest(final Activity activity) {
        if (cancelledAndCanRequest == null)
            cancelledAndCanRequest = "Permission required for this app, please grant all permission .";

        showDialogOK(activity, cancelledAndCanRequest, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        requestPermission(activity, mPerpermissionListener, mCustomPermission);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        mPerpermissionListener.onPermissionDenied(mPerpermissionListener);
                        // proceed with logic by disabling the related features or quit the app.
                        break;
                }
            }
        });
    }

    /**
     * forcefully stoped all permission dialog
     * @param activity
     */
    private static void ifCancelledAndCannotRequest(final Activity activity) {
        if (cancelledAndCannotRequest == null)
            cancelledAndCannotRequest = "You have forcefully denied some of the required permissions \n" + "for this action. Please go to permissions and allow them.";

        showDialogOK(activity, cancelledAndCannotRequest, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
                        intent.setData(uri);
                        activity.startActivity(intent);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        mPerpermissionListener.onPermissionDenied(mPerpermissionListener);
                        // proceed with logic by disabling the related features or quit the app.
                        break;
                }
            }
        });
    }

    private static void showDialogOK(Activity activity, String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(activity).setMessage(message).setPositiveButton("OK", okListener).setNegativeButton("Cancel", okListener).create().show();
    }
}
